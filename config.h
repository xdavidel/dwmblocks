//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/  /*Command*/     /*Update Interval*/  /*Update Signal*/
    {"",      "music",        0,                   11      },
    {"",      "uppackages",   0,                   8       },
    {"",      "news",         0,                   6       },
    {"",      "weather",      18000,               5       },
    {"",      "memory",       8,                   14      },
    {"",      "cpu",          10,                  13      },
    {"",      "audiovol",     0,                   10      },
    {"",      "network",      5,                   4       },
    {"",      "battery",      10,                  7       },
};

static const Block extrablocks[] = {
    /*Icon*/        /*Command*/             /*Update Interval*/     /*Update Signal*/
    {"",    "keyboardlayout",   0,                                  3       },
    {"",    "clock",                        60,                                     1       },
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
