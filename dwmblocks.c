#include <X11/Xlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define LENGTH(X) (sizeof(X) / sizeof(X[0]))
#define CMDLENGTH 50
#define BUTTON_ENV "BUTTON"

typedef struct {
  char *icon;
  char *command;
  unsigned int interval;
  unsigned int signal;
} Block;
void buttonhandler(int sig, siginfo_t *si, void *ucontext);
void replace(char *str, char old, char new);
void remove_all(char *str, char to_remove);
void getcmds(int time);
#ifndef __OpenBSD__
void getsigcmds(int signal);
void setupsignals();
void sighandler(int signum);
#endif
int getstatus(char *str, char *last);
void setroot();
void statusloop();
void termhandler(int signum);

#include "config.h"

static Display *dpy;
static int screen;
static Window root;
static char statusbar[LENGTH(blocks)][CMDLENGTH] = {0};
static char statusstr[2][256];
static char extrabar[LENGTH(extrablocks)][CMDLENGTH] = {0};
static int statusContinue = 1;
static void (*writestatus)() = setroot;

void replace(char *str, char old, char new) {
  int N = strlen(str);
  for (int i = 0; i < N; i++)
    if (str[i] == old)
      str[i] = new;
}

void remove_all(char *str, char to_remove) {
  char *read = str;
  char *write = str;
  while (*read) {
    if (*read == to_remove) {
      read++;
      *write = *read;
    }
    read++;
    write++;
  }
}

// opens process *cmd and stores output in *output
void getcmd(const Block *block, char *output) {
  if (block->signal) {
    output[0] = block->signal;
    output++;
  }
  strcpy(output, block->icon);
  char *cmd = block->command;
  FILE *cmdf = popen(cmd, "r");
  if (!cmdf)
    return;
  char c;
  int i = strlen(block->icon);
  fgets(output + i, CMDLENGTH - i, cmdf);
  remove_all(output, '\n');
  i = strlen(output);
  if (delim != '\0' && i)
    output[i++] = delim;
  output[i++] = '\0';
  pclose(cmdf);
}

void getcmds(int time) {
  const Block *current;
  for (int i = 0; i < LENGTH(blocks); i++) {
    current = blocks + i;
    if ((current->interval != 0 && time % current->interval == 0) || time == -1)
      getcmd(current, statusbar[i]);
  }
  for (int i = 0; i < LENGTH(extrablocks); i++) {
    current = extrablocks + i;
    if ((current->interval != 0 && time % current->interval == 0) || time == -1)
      getcmd(current, extrabar[i]);
  }
}

#ifndef __OpenBSD__
void getsigcmds(int signal) {
  const Block *current;
  for (int i = 0; i < LENGTH(blocks); i++) {
    current = blocks + i;
    if (current->signal == signal)
      getcmd(current, statusbar[i]);
  }
  for (int i = 0; i < LENGTH(extrablocks); i++) {
    current = extrablocks + i;
    if (current->signal == signal)
      getcmd(current, extrabar[i]);
  }
}

void setupsignals() {
  struct sigaction sa;
  for (int i = 0; i < LENGTH(blocks); i++) {
    if (blocks[i].signal > 0) {
      signal(SIGRTMIN + blocks[i].signal, sighandler);
      sigaddset(&sa.sa_mask, SIGRTMIN + blocks[i].signal);
    }
  }
  for (int i = 0; i < LENGTH(extrablocks); i++) {
    if (extrablocks[i].signal > 0) {
      signal(SIGRTMIN + extrablocks[i].signal, sighandler);
      sigaddset(&sa.sa_mask, SIGRTMIN + extrablocks[i].signal);
    }
  }
  sa.sa_sigaction = buttonhandler;
  sa.sa_flags = SA_SIGINFO;
  sigaction(SIGUSR1, &sa, NULL);
  signal(SIGCHLD, SIG_IGN);
}
#endif

int getstatus(char *str, char *last) {
  strcpy(last, str);
  int j = 0;
  for (int i = 0; i < LENGTH(blocks); j += strlen(statusbar[i++])) {
    strcpy(str + j, statusbar[i]);
  }

  /* Splitter for extra bar */
  j--;
  str[j - 1] = ';';

  for (int i = 0; i < LENGTH(extrablocks); j += strlen(extrabar[i++])) {
    strcpy(str + j, extrabar[i]);
  }

  str[--j] = 0;
  return strcmp(str, last); // 0 if they are the same
}

void setroot() {
  if (!getstatus(statusstr[0],
                 statusstr[1])) // Only set root if text has changed.
    return;
  Display *d = XOpenDisplay(NULL);
  if (d) {
    dpy = d;
  }
  screen = DefaultScreen(dpy);
  root = RootWindow(dpy, screen);
  XStoreName(dpy, root, statusstr[0]);
  XCloseDisplay(dpy);
}

void pstdout() {
  if (!getstatus(statusstr[0],
                 statusstr[1])) // Only write out if text has changed.
    return;
  printf("%s\n", statusstr[0]);
  fflush(stdout);
}

void statusloop() {
#ifndef __OpenBSD__
  setupsignals();
#endif
  int i = 0;
  getcmds(-1);
  while (statusContinue) {
    getcmds(i);
    writestatus();
    sleep(1.0);
    i++;
  }
}

#ifndef __OpenBSD__
void sighandler(int signum) {
  getsigcmds(signum - SIGRTMIN);
  writestatus();
}

void buttonhandler(int sig, siginfo_t *si, void *ucontext) {
  int button = si->si_value.sival_int & 0xff;
  sig = si->si_value.sival_int >> 8;
  getsigcmds(sig);
  writestatus();
  if (fork() == 0) {
    static char exportstring[CMDLENGTH + 22] = "export BLOCK_BUTTON=-;";
    const Block *current;
    int i;
    Bool blockfound = 0;
    for (i = 0; i < LENGTH(blocks); i++) {
      current = blocks + i;
      if (current->signal == sig) {
        blockfound = 1;
        break;
      }
    }

    if (!blockfound) {
      for (i = 0; i < LENGTH(extrablocks); i++) {
        current = extrablocks + i;
        if (current->signal == sig) {
          blockfound = 1;
          break;
        }
      }
    }

    if (blockfound) {
      char *cmd = strcat(exportstring, current->command);
      cmd[20] = '0' + button;
      char *command[] = {"/bin/sh", "-c", cmd, NULL};
      setsid();
      execvp(command[0], command);
      exit(EXIT_SUCCESS);
      cmd[22] = '\0';
    }
  }
}

#endif

void termhandler(int signum) {
  statusContinue = 0;
  exit(0);
}

int main(int argc, char **argv) {
  for (int i = 0; i < argc; i++) {
    if (!strcmp("-d", argv[i]))
      delim = argv[++i][0];
    else if (!strcmp("-p", argv[i]))
      writestatus = pstdout;
  }
  signal(SIGTERM, termhandler);
  signal(SIGINT, termhandler);
  statusloop();
}
